<?php

include 'db.inc.php';

if (!empty($_POST['uuid']) &&
    !empty($_POST['arch']) &&
    !empty($_POST['osversion']) &&
    !empty($_POST['pkglist'])) {

    $id =        filter_var($_POST['uuid'], FILTER_SANITIZE_STRING);
    $arch =      filter_var($_POST['arch'], FILTER_SANITIZE_STRING);
    $osversion = filter_var($_POST['osversion'], FILTER_SANITIZE_STRING);
    $pkglist =   filter_var($_POST['pkglist'], FILTER_SANITIZE_STRING);

} else { echo "Error, a param is missing"; print_r($_POST); die(); }

// we check if we already known this SHA256 UUID
$result = pg_query_params($dbconn, 'SELECT sys_id from sys where sys_uuid = $1', array($id));
$res = pg_fetch_array($result);

// if false => new system
if(! $res) {
  echo "Hello ! You are new ! Welcome :)\n";
  $result = pg_query_params($dbconn, 'INSERT INTO sys (sys_uuid, sys_arch, sys_version) values ($1,$2,$3) RETURNING sys_id',
          array($id, $arch, $osversion));
  $res = pg_fetch_array($result);
  $system_id = $res[0];
} else { // system known, we update it
  echo "Updating your registered package list\n";
  $system_id = $res[0];
  pg_query_params($dbconn, 'UPDATE sys set sys_arch = $1, sys_version = $2 where sys_uuid = $3',
          array($arch, $osversion, $id));
}

// we remove every package for this system
pg_query_params($dbconn, 'DELETE from packages where pkg_sys = $1', array($system_id));

// we insert the packages
foreach(preg_split("/\n/", $pkglist) as $pkg) {
  if(strlen($pkg) > 1) {
    $tab[] = $pkg."\t".$system_id;
  }
}

pg_copy_from($dbconn,"packages",$tab);


?>
