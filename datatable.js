var datatable_parse_date = Date.parse,
    datatable_parse_float = parseFloat,
    datatable_parse_int = parseInt,
    datatable_parse_string = String;

function datatable_sort_default(x, y) {
	return x > y ? 1 : (x == y ? 0 : -1);
}

function datatable_init(el) {
	var thead = el.tHead,
	    tbody = el.tBodies[0];
	var ths = thead.children[0].children,
	    cols = [];
	for (var i = 0; i < ths.length; i++)
		cols.push({
			parsefn:    window["datatable_parse_" + (ths[i].getAttribute("data-parse") || "string")],
			sortfn:     window["datatable_sort_" + (ths[i].getAttribute("data-sort")   || "default")],
			sortable:   ["1", "true"].indexOf(ths[i].getAttribute("data-sortable")     || "true") != -1
		});
	var d = {
		table: el,
		thead: thead,
		ths:   ths,
		tbody: tbody,
		cols:  cols,
		sort:  [], // sort options: [colidx, order (ASC = 0, DESC = 1)].
		search: "" // previous search text.
	};
	d.data = datatable_data_parse(d);

	d.display = function(data) {
		var tbody = document.createElement("tbody");
		for (var i = 0; i < data.length; i++)
			tbody.appendChild(data[i].tr);
		d.table.replaceChild(tbody, d.tbody);
		tbody.style.display = data.length ? "table-row-group" : "none";
		d.tbody = tbody;
	};

	// setup click event handlers for sorting.
	for (var i = 0; i < d.ths.length; i++)
		d.cols[i].sortable && d.ths[i].addEventListener("click", function(idx) {
			return function(e) {
				// shift-click for multi-select modifier.
				datatable_sort_column_toggle(d, idx, e.shiftKey);
				d.data = datatable_sort(d, d.data);
				d.display(d.data);
			};
		}(i), false);
	return d;
}

function datatable_sort_column_get(d, idx) {
	for (var i = 0; i < d.sort.length; i++)
		if (d.sort[i][0] == idx)
			return i;
	return -1;
}

function datatable_sort_column_set(d, idx, order, multi) {
	var c = datatable_sort_column_get(d, idx);
	if (multi)
		if (c != -1)
			d.sort[c][1] = order;
		else
			d.sort.push([ idx, order ]);
	else
		d.sort = [ [idx, order] ];

	for (var i = 0; i < d.ths.length; i++) {
		var c = " " + d.ths[i].className + " ";
		d.ths[i].className = c.replace(/ sort-(asc|desc) /g, " ").replace(/\s+/g, " ").trim();
	}
	for (var i = 0; i < d.sort.length; i++)
		d.ths[d.sort[i][0]].className += " sort-" + (d.sort[i][1] ? "desc" : "asc");
}

// toggle sort or use default order: ASC.
function datatable_sort_column_toggle(d, idx, multi) {
	var c = datatable_sort_column_get(d, idx);
	datatable_sort_column_set(d, idx, c == -1 || d.sort[c][1] ? 0 : 1, multi);
}

function datatable_data_parse(d) {
	var data = [], trs = d.tbody.children;
	// NOTE: assumes each tr has only "<td>" childnodes.
	for (var i = 0; i < trs.length; i++) {
		var values = [];
		for (var j = 0, trc = trs[i].children; j < trc.length; j++) {
			var td = trc[j], v = td.getAttribute("data-value");
			// prefer data-value attribute, else use cell contents,
			// also set preprocess values to filter on cell content
			// and data-value (case-insensitive).
			if (typeof(v) != "undefined" && v !== null)
				values.push(d.cols[j].parsefn(v));
			else
				values.push(d.cols[j].parsefn(td.textContent || td.innerText));
		}
		data.push({
			tr:     trs[i],
			values: values
		});
	}
	return data;
}

function datatable_sort(d, data) {
	// setup sort functions once (in order for multi-select).
	var sortfns = d.sort.map(function(s) {
		return (function(c, o, fn) {
			if (o)
				return function(xvals, yvals) {
					return -fn(xvals[c], yvals[c]);
				};
			else
				return function(xvals, yvals) {
					return fn(xvals[c], yvals[c]);
				};
		})(s[0], s[1], d.cols[s[0]].sortfn);
	});
	return data.sort(function(x, y) {
		for (var i = 0, r; i < sortfns.length; i++)
			if ((r = sortfns[i](x.values, y.values)) != 0)
				return r;
		return r;
	});
}

function datatable_autoload() {
	// convert to Array (not changed in-place, mandatory).
	var ds = [], dl = [], els = document.getElementsByClassName && document.getElementsByClassName("datatable") || [];
	for (var i = 0; i < els.length; i++)
		dl.push(els[i]);
	for (var i = 0, d; i < dl.length; i++) {
		if ((d = datatable_init(dl[i])) === null)
			continue;
		ds.push(d);
	}
	return ds;
}
