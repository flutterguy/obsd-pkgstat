#!/bin/sh

(
for line in $(psql -d opkgstat -t -A -c "select string_agg(pkg_name,','), pkg_sys from packages GROUP BY pkg_sys;" | cut -d '|' -f 1)
do
    echo $line | sha256
done
) | sort | uniq -c | sort -n | awk '{ if($1 != 1) { print $0 }}'
